﻿using FluentNHibernate.Mapping;
using loan.Models.Calculators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Mappers.Calculators
{
    public class SimulationMapper : ClassMap<Simulation>
    {
        public SimulationMapper()
        {
            Table("t_simulation");
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.PropertyPrices).Not.Nullable();
            Map(x => x.PTKP).Not.Nullable();
            Map(x => x.DownPayment).Not.Nullable();
            Map(x => x.LoanInterestCost).Not.Nullable();
            Map(x => x.HomeInsuranceCost).Not.Nullable();
            Map(x => x.LifeInsuranceCost).Not.Nullable();
            Map(x => x.AdministrativeCost).Not.Nullable();
            Map(x => x.BPHTB).Not.Nullable();
            Map(x => x.NotaryFee).Not.Nullable();
            Map(x => x.PNPB).Not.Nullable();
            Map(x => x.StampDuty).Not.Nullable();
        }
    }
}
