﻿using FluentNHibernate.Mapping;
using loan.Models.Calculators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Mappers.Calculators
{
    public class PTKPMapper : ClassMap<PTKP>
    {
        public PTKPMapper()
        {
            Table("t_ptkp");
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.Status).Not.Nullable();
            Map(x => x.MateHasIncome).Not.Nullable();
            Map(x => x.Burden).Not.Nullable();
            Map(x => x.TotalPTKP).Not.Nullable();
        }
    }
}
