﻿using FluentNHibernate.Mapping;
using loan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Mappers
{
    public class VillageMapper : ClassMap<Village>
    {
        public VillageMapper()
        {
            Table("t_village");
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.VillageName).Not.Nullable();
            References<PostalCode>(x => x.PostalCodeId).Not.Nullable();
        }
    }
}
