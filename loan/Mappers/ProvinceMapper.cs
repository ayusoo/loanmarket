﻿using FluentNHibernate.Mapping;
using loan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Mappers
{
    public class ProvinceMapper : ClassMap<Province>
    {
        public ProvinceMapper()
        {
            Table("t_province");
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.ProvinceName).Not.Nullable();
            HasMany(x => x.Cities)
                .Inverse()
                .AsBag()
                .LazyLoad();
        }
    }
}
