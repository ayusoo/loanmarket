
    alter table t_city 
        add constraint FKECF515D760B0EF0F 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKD0A074B219BB3B38 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKB1C1BA13E540AFE5 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK1F3B804AB4D57BD 
        foreign key (PostalCodeId_id) 
        references t_postalcode
    alter table t_village 
        add constraint FK1F3B804A18AE56FC 
        foreign key (SubDistrict_id) 
        references t_subdistrict