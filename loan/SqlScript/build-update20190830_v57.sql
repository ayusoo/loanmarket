
    create table t_percent (
        Id  bigserial,
       CostName varchar(255) not null,
       PercentNumber float8 not null,
       primary key (Id)
    )
    alter table t_city 
        add constraint FKD8AC08023805A6A9 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK3AEBE0FAAA8E1C1A 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKCBB59620B985AB2A 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK63F0CDF9B0AE466 
        foreign key (PostalCodeId_id) 
        references t_postalcode