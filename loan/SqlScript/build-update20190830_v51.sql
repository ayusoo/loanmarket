
    alter table t_city 
        add constraint FK6D4A63F171D2B111 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK2DB3F957ACA38685 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK778ACFED30224E52 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKEE6B4DAA4176B91A 
        foreign key (PostalCodeId_id) 
        references t_postalcode