
    alter table t_city 
        add constraint FK32BCA7D4AF0B5823 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKA1109F30BA9B88B2 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKB309C8E9DF82B077 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKD951A3E7D463C206 
        foreign key (PostalCodeId_id) 
        references t_postalcode