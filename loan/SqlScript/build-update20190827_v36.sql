
    alter table t_city 
        add constraint FK88B6D402D20F2D5 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKC7ED929E8C1A81F9 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKF59FD070527A9E4D 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKAC49F48A8D08E7E 
        foreign key (PostalCodeId_id) 
        references t_postalcode