
    alter table t_city 
        add constraint FKD8C23E468C5C629D 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKF18BF87FC5BBC80 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKEBE7B2D8AAF1EF49 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK553D770C8D6EEAE2 
        foreign key (PostalCodeId_id) 
        references t_postalcode