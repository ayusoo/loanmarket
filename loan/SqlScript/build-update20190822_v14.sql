
    alter table t_city 
        add constraint FKB514B412B0438489 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK41BF21B49D121029 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK99A2BBC76ED053E3 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK7E5890CC7D0979E4 
        foreign key (PostalCodeId_id) 
        references t_postalcode
    alter table t_village 
        add constraint FK7E5890CCEF7ADA69 
        foreign key (SubDistrict_id) 
        references t_subdistrict