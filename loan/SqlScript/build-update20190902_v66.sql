
    alter table t_city 
        add constraint FKC83A22DF180128A5 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKDA117D81955382B4 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK389AB99DE3EB94B4 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK6A30892AE40D018B 
        foreign key (PostalCodeId_id) 
        references t_postalcode