
    alter table t_city 
        add constraint FK3D75BA566F28EA 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK71495FD5BD31BFC2 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK7649829812AD5853 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK8430A9D2F8251C79 
        foreign key (PostalCodeId_id) 
        references t_postalcode