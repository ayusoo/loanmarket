
    alter table t_city 
        add constraint FK2181112493FBFBAC 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK58DF946274F7F499 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKB57E03DEAF4BDE7D 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKF1583D94EF1DE5C3 
        foreign key (PostalCodeId_id) 
        references t_postalcode
    alter table t_village 
        add constraint FKF1583D946C7ACCCA 
        foreign key (SubDistrict_id) 
        references t_subdistrict