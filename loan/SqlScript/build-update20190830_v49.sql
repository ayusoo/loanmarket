
    create table t_ptkp (
        Id  bigserial,
       Status boolean not null,
       MateHasIncome boolean not null,
       Burden int2 not null,
       TotalPTKP int8 not null,
       primary key (Id)
    )
    alter table t_city 
        add constraint FK2F46EC3E344F79DD 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK82FF280C361BA39B 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK76703A90349D516 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKB1DF175D5BA28011 
        foreign key (PostalCodeId_id) 
        references t_postalcode