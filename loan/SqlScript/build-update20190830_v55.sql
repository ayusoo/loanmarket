
    create table t_defaultcost (
        Id  bigserial,
       CostName varchar(255) not null,
       Cost int8 not null,
       primary key (Id)
    )
    create table t_percent (
        Id  bigserial,
       CostName varchar(255) not null,
       PercentNumber int4 not null,
       primary key (Id)
    )
    alter table t_city 
        add constraint FK1280F6187C919B2C 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKC69C94EF511E6A48 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK9FD8E165CDBE1E88 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK67161362EBC49143 
        foreign key (PostalCodeId_id) 
        references t_postalcode