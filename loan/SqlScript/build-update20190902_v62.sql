
    create table t_percent (
        Id  bigserial,
       CostName varchar(255) not null,
       PercentNumber decimal(19,5) not null,
       primary key (Id)
    )
    create table t_simulation (
        Id  bigserial,
       PropertyPrices int8 not null,
       PTKP int8 not null,
       DownPayment int8 not null,
       LoanInterestCost decimal(19,5) not null,
       HomeInsuranceCost decimal(19,5) not null,
       LifeInsuranceCost int8 not null,
       AdministrativeCost decimal(19,5) not null,
       BPHTB decimal(19,5) not null,
       NotaryFee decimal(19,5) not null,
       PNPB int8 not null,
       StampDuty int8 not null,
       primary key (Id)
    )
    alter table t_city 
        add constraint FK32FC2E1696E6C762 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKEE278FD5A9EF88A8 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK2F0E93D7DE134D40 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKDAB43CABD46E0AF8 
        foreign key (PostalCodeId_id) 
        references t_postalcode