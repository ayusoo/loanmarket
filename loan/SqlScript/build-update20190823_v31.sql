
    alter table t_city 
        add constraint FK8C55F98AEB9C2383 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKEB7D57026536772E 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKF67D4FEA7CEC5BAA 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK20C0ACD88BA3442 
        foreign key (PostalCodeId_id) 
        references t_postalcode