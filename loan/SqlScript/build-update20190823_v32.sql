
    alter table t_city 
        add constraint FK69D5EFFB54AB2CC9 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKAD36E04D349B5EEE 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK2B3E827D1599D7F 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKED182900792F9C13 
        foreign key (PostalCodeId_id) 
        references t_postalcode