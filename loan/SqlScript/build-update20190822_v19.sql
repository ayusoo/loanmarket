
    alter table t_city 
        add constraint FK29693AD26477FBB8 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKE6B28137A022FBDA 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK71061E15A752F6CA 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK7CF4D22BCCB672E 
        foreign key (PostalCodeId_id) 
        references t_postalcode