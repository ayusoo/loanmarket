
    alter table t_city 
        add constraint FK999663B22F1BFBC0 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKBB2F3A6E81358E75 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK5751A00BDC50A34D 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK25A09CE7111027D 
        foreign key (PostalCodeId_id) 
        references t_postalcode