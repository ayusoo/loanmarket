
    alter table t_city 
        add constraint FKE5BED2C6BAF96B89 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKC94FDD39460DF600 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKB28E480EE8B92117 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK2530F538188C41AB 
        foreign key (PostalCodeId_id) 
        references t_postalcode