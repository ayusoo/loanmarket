
    alter table t_city 
        add constraint FKB7E31D728F25B911 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK96A114DC11C22F2 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK63C306966099AE7E 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK25B394964CB4E966 
        foreign key (PostalCodeId_id) 
        references t_postalcode