
    alter table t_city 
        add constraint FK3C584F8547CAF41 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK9FE268591EEF0A88 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK9350C1CF86EBF629 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK3C53AA75D7DE09F6 
        foreign key (PostalCodeId_id) 
        references t_postalcode