
    alter table t_city 
        add constraint FKA1ED3D49FA1CC31E 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK991A5AC95AA367BB 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK8B141C114A6D9C4A 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKF42BAB8FBC831BB4 
        foreign key (PostalCodeId_id) 
        references t_postalcode