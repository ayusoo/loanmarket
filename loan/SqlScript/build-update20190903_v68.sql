
    alter table t_city 
        add constraint FK9CAAC0B8219135A5 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK29AABE5A19B3E91B 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK86F5912D556EEA93 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK25A297786DEF3396 
        foreign key (PostalCodeId_id) 
        references t_postalcode