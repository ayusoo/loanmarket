
    alter table t_city 
        add constraint FK1DD59FBCC5CE7724 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKD821DB59B84035B9 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK87D90276C3CAE9CB 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK393217A966F4D759 
        foreign key (PostalCodeId_id) 
        references t_postalcode