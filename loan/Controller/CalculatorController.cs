﻿using loan.Commands;
using loan.Dtos.Calculators;
using loan.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class CalculatorController : ControllerBase
    {
        public ICalculatorService calculatorService;

        public CalculatorController(ICalculatorService calculatorService)
        {
            this.calculatorService = calculatorService;
        }

        [HttpGet]
        [Route("PTKP")]
        [AllowAnonymous]
        public IList<PTKPDto> GetAllPTKP()
        {
            return calculatorService.GetAllPTKP();
        }

        [HttpPost]
        [Route("Simulation")]
        [AllowAnonymous]
        public void AddNewCity([FromBody] SimulationCommand command)
        {
            this.calculatorService.SimulationResult(command);
        }

        [HttpGet]
        [Route("Simulation")]
        [AllowAnonymous]
        public IList<SimulationDto> GetAllSimulation()
        {
            return calculatorService.GetAllSimulation();
        }

        [HttpPut]
        [Route("HomeInsuranceCost")]
        [AllowAnonymous]
        public void EditHomeInsuranceCost([FromBody]EditHomeInsuranceCommand command)
        {
            this.calculatorService.EditHomeInsuranceCost(command);
        }

        [HttpPut]
        [Route("LifeInsuranceCost")]
        [AllowAnonymous]
        public void EditLifeInsuranceCost([FromBody]EditLifeInsuranceCommand command)
        {
            this.calculatorService.EditLifeInsuranceCost(command);
        }
    }
}
