﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Commands
{
    public class EditHomeInsuranceCommand
    {
        public long Id { set; get; }
        public decimal NewCost { set; get; }
    }
}
