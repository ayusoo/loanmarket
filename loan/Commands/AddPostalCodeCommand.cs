﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Commands
{
    public class AddPostalCodeCommand
    {
        public string PostalCodeNumber { set; get; }
        public long SubDistrictId { set; get; }
    }
}
