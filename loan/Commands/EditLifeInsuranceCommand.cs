﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Commands
{
    public class EditLifeInsuranceCommand
    {
        public long Id { set; get; }
        public long NewCost { set; get; }
    }
}
