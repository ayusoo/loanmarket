﻿using loan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Dtos
{
    public class PostalCodeDto
    {
        public long Id { set; get; }
        public string PostalCodeNumber { set; get; }

        public PostalCodeDto()
        {

        }

        public PostalCodeDto(long id, string postalCodeNumber)
        {
            Id = id;
            PostalCodeNumber = postalCodeNumber;
        }
    }
}
