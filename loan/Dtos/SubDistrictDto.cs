﻿using loan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Dtos
{
    public class SubDistrictDto
    {
        public long Id { set; get; }
        public string SubDistrictName { set; get; }

        public SubDistrictDto()
        { }

        public SubDistrictDto(long id, string subDistrictName)
        {
            Id = id;
            SubDistrictName = subDistrictName;
        }

    }
}
